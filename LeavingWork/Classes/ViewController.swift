//
//  ViewController.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/29/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIAlertViewDelegate, NSURLConnectionDelegate, ServerWrapperDelegate, UITextFieldDelegate
{
    var receivedData = NSMutableData()
    var results = NSDictionary()
    var constants = Constants()
    
    // Outlets
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var progressIndicatingView: UIView!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var acceptPolicyButton: UIButton!
    
    
    // MARK - ViewLifeCycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        acceptPolicyButton.selected = true
        submitButton.layer.cornerRadius = 6.0
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK - IBActions
    
    @IBAction func termsOfServicePressed(sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let webViewController = storyboard.instantiateViewControllerWithIdentifier("webViewIdentifier") as! UIViewController
        self.presentViewController(webViewController, animated: true, completion: nil)
    }
    
    @IBAction func dismissKeyboardPressed(sender: AnyObject)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func SignUpPressed(sender: AnyObject)
    {
        emailField.text = emailField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        passwordField.text = passwordField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        nameField.text = nameField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        var alertMessage : NSString = "";
        
        if nameField.text.isEmpty
        {
            alertMessage = constants.KEnterName
        }
        else if emailField.text.isEmpty
        {
            alertMessage = constants.KEnterEmail
        }
        else if !Helper.isValidEmail(emailField.text)
        {
            alertMessage = constants.kEnterValidEmail
        }
        else if passwordField.text.isEmpty
        {
            alertMessage = constants.kEnterPassword
        }
        else if acceptPolicyButton.selected == false
        {
            alertMessage = constants.kAcceptTerms
        }

        if alertMessage.length > 0
        {
            Helper.showAlertDialog(constants.kError, alertMessage: alertMessage)
            return
        }
        
        if Reachability.reachabilityForInternetConnection().isReachable() == false
        {
            Helper.showInternetConnectionWarning()
            return
        }
        
        // send request to server to create account
        Helper.GetAppDelegate().backend.createAccount(nameField.text, emailId: emailField.text, password: passwordField.text, delegate: self)
        progressIndicatingView.hidden = false
        self.view.endEditing(true)
    }
    
    @IBAction func checkMarkButtonPressed(sender: AnyObject)
    {
        if acceptPolicyButton.selected
        {
            acceptPolicyButton.selected = false
        }
        else
        {
            acceptPolicyButton.selected = true
        }
    }
    
    // MARK - ServerWrapper Delegate Methods
    
    func registrationCompleted(results: NSDictionary)
    {
        println("registration completed \(results)")
        
        progressIndicatingView.hidden = true
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if results.objectForKey(constants.kServerEmail) != nil
        {
            defaults.setObject(results.objectForKey(constants.kServerEmail), forKey: constants.kUserEmail)
        }
        
        if results.objectForKey(constants.kUserName) != nil
        {
            defaults.setObject(results.objectForKey(constants.kUserName), forKey: constants.kUserName)
        }
        
        if results.objectForKey(constants.kSession) != nil
        {
            defaults.setObject(results.objectForKey(constants.kSession), forKey: constants.kSession)
        }
        
        if results.objectForKey(constants.kToken) != nil
        {
            defaults.setObject(results.objectForKey(constants.kToken), forKey: constants.kToken)
        }
        
        defaults.setBool(true, forKey: constants.kRegistrationDone)
        
        defaults.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let locationPermissionController = storyboard.instantiateViewControllerWithIdentifier("LocationPermision") as! LocationPermissionScreen
        Helper.GetAppDelegate().window?.rootViewController = locationPermissionController
    }
    
    func requestFailed(requestId: Int, errorInfo: NSDictionary)
    {
        println("requestFailed with Error \(errorInfo)")
        progressIndicatingView.hidden = true
        
        var errMsg = errorInfo.objectForKey(constants.kMessage) as! String
        Helper.showAlertDialog(constants.kError, alertMessage: errMsg)
    }
    
    // MARK: TextField Delegate Methods
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
}


