//
//  SetPhoneScreen.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/30/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation
import UIKit

enum PICKER_TAG: Int
{
    case START_TIME_PICKER_TAG  = 1
    case END_TIME_PICKER_TAG    = 2
}

class SetPhoneScreen: UIViewController
{
    var constants = Constants()
    var startDate = NSDate();
    var endDate = NSDate();
    var countryCodes = NSDictionary()
    
    // Outlets
    @IBOutlet var phoneField: UITextField!
    @IBOutlet var dialingCodeTextField: UITextField!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var datePickerView: UIView!
    @IBOutlet var timePicker: UIDatePicker!
    @IBOutlet var fromTimeLabel: UILabel!
    @IBOutlet var toTimeLabel: UILabel!
    @IBOutlet var progressIndicatingView: UIView!
    
    @IBOutlet var DoneButton: UIButton!
    @IBOutlet var pickerCancelButton: UIButton!
    @IBOutlet var pickerDoneButton: UIButton!
    @IBOutlet var timePickerTitle: UILabel!
    
    // MARK - ViewLifeCycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        DoneButton.layer.cornerRadius = 6.0
        
        fromTimeLabel.layer.borderWidth = 1.0
        fromTimeLabel.layer.borderColor = UIColor(red: (190/255), green: (190/255), blue: (190/255), alpha: 1.0).CGColor

        toTimeLabel.layer.borderWidth = 1.0
        toTimeLabel.layer.borderColor = UIColor(red: (190/255), green: (190/255), blue: (190/255), alpha: 1.0).CGColor
        
        dialingCodeTextField.layer.cornerRadius = 2.0
        dialingCodeTextField.layer.borderColor = UIColor(red: (239/255), green: (239/255), blue:(239/255), alpha: 1.0).CGColor
        dialingCodeTextField.layer.borderWidth = 1.0
        
        phoneField.layer.cornerRadius = 2.0
        phoneField.layer.borderColor = UIColor(red: (239/255), green: (239/255), blue:(239/255), alpha: 1.0).CGColor
        phoneField.layer.borderWidth = 1.0
        
        if let path = NSBundle.mainBundle().pathForResource("DiallingCodes", ofType: "plist")
        {
            countryCodes = NSDictionary(contentsOfFile: path)!
        }
        
        var calendar = NSCalendar.currentCalendar()
        
        var dateComponenets = NSDateComponents()
        dateComponenets.hour = 16
        startDate = calendar.dateFromComponents(dateComponenets)!
        
        dateComponenets.hour = 19
        endDate = calendar.dateFromComponents(dateComponenets)!
        
        messageTextView.text = constants.kUserTextMessage // TO_DO (Name of User)
        messageTextView.layer.borderColor = UIColor(red: (239/255), green: (239/255), blue:(239/255), alpha: 1.0).CGColor
        messageTextView.layer.borderWidth = 0.5
        datePickerView.hidden = true
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if defaults.objectForKey(constants.kUserInfo) != nil
        {
            var userInfo: NSDictionary = defaults.objectForKey(constants.kUserInfo) as! NSDictionary
            
            if userInfo.objectForKey(constants.kUserPhone) != nil
            {
                phoneField.text = userInfo.objectForKey(constants.kUserPhone) as! String
            }
            
            if userInfo.objectForKey(constants.kUserPhoneDC) != nil
            {
                dialingCodeTextField.text = userInfo.objectForKey(constants.kUserPhoneDC) as! String
            }
            
            if userInfo.objectForKey(constants.ksmsText) != nil
            {
                messageTextView.text = userInfo.objectForKey(constants.ksmsText) as! String
            }
            
            if userInfo.objectForKey(constants.kStartTime) != nil
            {
                startDate = userInfo.objectForKey(constants.kStartTime) as! NSDate
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = constants.kTimeFormat
                let customDateStr = dateFormatter.stringFromDate(startDate)
                
                 fromTimeLabel.text = "\(customDateStr) "
            }
            
            if userInfo.objectForKey(constants.kEndTime) != nil
            {
                endDate = userInfo.objectForKey(constants.kEndTime) as! NSDate
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = constants.kTimeFormat
                let customDateStr = dateFormatter.stringFromDate(endDate)
                
                toTimeLabel.text = "\(customDateStr)"
            }
        }
        else
        {
            var countryCode : String = (NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as! String?)!
            
            countryCode = countryCode.lowercaseString
            var countryCodeStr : NSString = countryCodes.objectForKey(countryCode) as! NSString
            
            dialingCodeTextField.text = "+\(countryCodeStr)"
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = constants.kTimeFormat
            
            let customStartDateStr = dateFormatter.stringFromDate(startDate)
            
            fromTimeLabel.text = "\(customStartDateStr) "
        
            let customEndDateStr = dateFormatter.stringFromDate(endDate)
            
            toTimeLabel.text = "\(customEndDateStr)"
        }
    }
    
    // MARK - IBActions
    
    @IBAction func donePressed(sender: AnyObject)
    {
        phoneField.text = phoneField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        dialingCodeTextField.text = dialingCodeTextField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        messageTextView.text = messageTextView.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        var alertMessage = NSString();
        
        if dialingCodeTextField.text.isEmpty
        {
            alertMessage = constants.kEnterPhoneDC
        }
        else if phoneField.text.isEmpty
        {
            alertMessage = constants.kEnterPhone
        }
        else if messageTextView.text.isEmpty
        {
            alertMessage = constants.kEnterSMSText
        }
        
        if alertMessage.length > 0
        {
            let msg = alertMessage as String
            Helper.showAlertDialog(constants.kError, alertMessage: alertMessage)
            return
        }
        
        self.view.endEditing(true)
        
        saveDataAndRegisterRecipe(phoneField.text, diailingCode: dialingCodeTextField.text, message: messageTextView.text, startDate: startDate, endDate: endDate)
    }
    
    func saveDataAndRegisterRecipe(phone: NSString, diailingCode: NSString, message: NSString, startDate : NSDate, endDate: NSDate)
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        let userInfo = [constants.kUserPhone : phone, constants.kUserPhoneDC : diailingCode, constants.ksmsText : message, constants.kStartTime : startDate, constants.kEndTime : endDate]
        defaults.setObject(userInfo, forKey: constants.kUserInfo)
        
        defaults.setBool(true, forKey: constants.kRecipeRegistrationDone)
        defaults.synchronize()
        
        // Show Confirmation Screen
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
        Helper.GetAppDelegate().window?.rootViewController = confirmationScreen
        confirmationScreen.viewDidAppear(false)
        
        Helper.GetAppDelegate().registeringRecipe()
    }

    
    @IBAction func dismissButtonPressed(sender: AnyObject)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func pickFromTimePressed(sender: AnyObject)
    {
        self.view.endEditing(true)
        
        timePickerTitle.text = constants.kSetStartTime
        timePicker.tag = PICKER_TAG.START_TIME_PICKER_TAG.rawValue
        timePicker.setDate(startDate, animated: true)

        datePickerView.hidden = false
    }
    
    @IBAction func pickToTimePressed(sender: AnyObject)
    {
        self.view.endEditing(true)
        
        timePickerTitle.text = constants.kSetEndTime
        timePicker.tag = PICKER_TAG.END_TIME_PICKER_TAG.rawValue
        timePicker.setDate(endDate, animated: true)
        
        datePickerView.hidden = false
    }

    @IBAction func datePickerCancelPressed(sender: AnyObject)
    {
        datePickerView.hidden = true
    }
    
    @IBAction func datePickerDismissPressed(sender: AnyObject)
    {
        self.view.endEditing(true)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = constants.kTimeFormat
        let customDateStr = dateFormatter.stringFromDate(timePicker.date)
        let date = dateFormatter.dateFromString(customDateStr)
            
        if timePicker.tag == PICKER_TAG.START_TIME_PICKER_TAG.rawValue
        {
            fromTimeLabel.text = "\(customDateStr)"
            startDate = timePicker.date
        }
        else if timePicker.tag == PICKER_TAG.END_TIME_PICKER_TAG.rawValue
        {
            toTimeLabel.text = "\(customDateStr)"
            endDate = timePicker.date
        }
        datePickerView.hidden = true
    }
}
