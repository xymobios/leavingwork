//
//  CustomWebView.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/29/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation
import UIKit

class CustomWebView: UIViewController, UIWebViewDelegate
{
    var constants = Constants()
    // Outlets
    @IBOutlet var webView: UIWebView!
    
    // MARK - ViewLifeCycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
     
        let url  = NSURL (string: constants.URL_TERMS_OF_USE) 
        let request = NSURLRequest(URL: url!)
        webView.loadRequest(request)
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(true)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    // MARK - IBActions
    
    @IBAction func closeButtonPressed(sender: AnyObject)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK - WebViewDelegate Methods

    func webViewDidStartLoad(webView: UIWebView)
    {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(webView: UIWebView)
    {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError)
    {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
}