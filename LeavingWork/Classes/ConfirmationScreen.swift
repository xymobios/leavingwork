//
//  ConfirmationScreen.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 6/1/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation
import UIKit
import SenseSdk

enum BUTTON_TAG : Int
{
    case PAUSE      =   100
    case RESTART    =   200
}

class ConfirmationScreen: UIViewController
{
    var constants = Constants()
    @IBOutlet var pauseOrRestartButton: UIButton!
    @IBOutlet var editButton: UIButton!
    
    @IBOutlet var notificationLabel: UILabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        editButton.layer.cornerRadius = 6.0
        editButton.layer.borderWidth = 1.0
        editButton.layer.borderColor = UIColor(red: (163/255), green: (163/255), blue: (163/255), alpha: 1.0).CGColor
        pauseOrRestartButton.layer.cornerRadius = 6.0
    }
    
    override func viewDidAppear(animated: Bool)
    {        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        // update button UI
        if defaults.boolForKey(constants.kSensingPaused)
        {
            if let button = pauseOrRestartButton
            {
                pauseOrRestartButton.setTitle(constants.kRestart, forState: UIControlState.Normal)
                pauseOrRestartButton.tag = BUTTON_TAG.RESTART.rawValue
            }
            
            if let label = notificationLabel
            {
                notificationLabel.text = "If you want to re-start the app, you can hit the button below"
            }
        }
        else
        {
            if let button = pauseOrRestartButton
            {
                pauseOrRestartButton.setTitle(constants.kPause, forState: UIControlState.Normal)
                pauseOrRestartButton.tag = BUTTON_TAG.PAUSE.rawValue
            }
            
            if let label = notificationLabel
            {
                notificationLabel.text = "If you want to pause the app, you can hit the button below"
            }
        }
    }
    
    @IBAction func pauseOrRestartPressed(sender: AnyObject)
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if pauseOrRestartButton.tag == BUTTON_TAG.PAUSE.rawValue
        {
            pauseOrRestartButton.setTitle(constants.kRestart, forState: UIControlState.Normal)
            pauseOrRestartButton.tag = BUTTON_TAG.RESTART.rawValue
            SenseSdk.unregister(name: constants.kWorkTriggerRecipe)
            defaults.setBool(true, forKey: constants.kSensingPaused)
            if let label = notificationLabel
            {
                notificationLabel.text = "If you want to re-start the app, you can hit the button below"
            }
        }
        else if pauseOrRestartButton.tag == BUTTON_TAG.RESTART.rawValue
        {
            pauseOrRestartButton.setTitle(constants.kPause, forState: UIControlState.Normal)
            pauseOrRestartButton.tag = BUTTON_TAG.PAUSE.rawValue
            Helper.GetAppDelegate().registeringRecipe()
            if let label = notificationLabel
            {
                notificationLabel.text = "If you want to pause the app, you can hit the button below"
            }
        }
        
        defaults.synchronize()
    }
    
    @IBAction func editButtonPressed(sender: AnyObject)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let phoneSetScreen = storyboard.instantiateViewControllerWithIdentifier("phoneScreen") as! UIViewController
        appDelegate.window?.rootViewController = phoneSetScreen
    }
}