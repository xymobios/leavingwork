//
//  LocationPermissionScreen.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/29/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class LocationPermissionScreen : UIViewController, CLLocationManagerDelegate
{
    var constants = Constants()
    
    @IBOutlet var shareLocationButton: UIButton!
    // MARK - view Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        shareLocationButton.layer.cornerRadius = 6.0
    }
    
    override func viewDidAppear(animated: Bool)
    {
    }

    // MARK - User defined Functions
    
    func checkAuthorizationStatus()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        switch CLLocationManager.authorizationStatus()
        {
            case CLAuthorizationStatus.AuthorizedAlways:
                NSLog("CLAuthorizationStatus > Authorized")
            case CLAuthorizationStatus.NotDetermined:
                appDelegate.manager.requestAlwaysAuthorization()
            case CLAuthorizationStatus.AuthorizedWhenInUse, CLAuthorizationStatus.Restricted, CLAuthorizationStatus.Denied:
                let alertController = UIAlertController(title: constants.kBGAccessDisbaledTitle,
                    message: constants.kLocationDeniedMsg, preferredStyle: UIAlertControllerStyle.Alert)
                
                let cancelAction = UIAlertAction(title: constants.kCancel,
                    style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
                })
                alertController.addAction(cancelAction)
                
                let openAction = UIAlertAction(title: constants.kOpenSettings,
                    style: UIAlertActionStyle.Default,
                    handler: { (action) -> Void in
                        if let url = NSURL(string: UIApplicationOpenSettingsURLString)
                        {
                            UIApplication.sharedApplication().openURL(url)
                        }
                })
                alertController.addAction(openAction)
                appDelegate.window?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    @IBAction func privacyPromisePressed(sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let webViewController = storyboard.instantiateViewControllerWithIdentifier("webViewIdentifier") as! UIViewController
        self.presentViewController(webViewController, animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject)
    {
        Helper.showAlertDialog("", alertMessage: "LeavingWork requires location to function.")
    }
    
    @IBAction func shareLocationButtonPressed(sender: AnyObject)
    {
        checkAuthorizationStatus()
    }
}