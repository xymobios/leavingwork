//
//  Helper.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 6/2/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit
import AdSupport

public class Helper: NSObject
{
    class func GetAppDelegate() -> AppDelegate
    {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
    // device token
    public class func token() -> NSString
    {
        var token = NSString()
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let tokenValue =  defaults.objectForKey(Constants().kToken) as? String
        {
            token = tokenValue
        }
        return token
    }
    
    // session
    public class func sessionValue() -> NSString
    {
        var session  = NSString()
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let sessionValue =  defaults.objectForKey(Constants().kSession) as? String
        {
            session = sessionValue
        }
        return session
    }
    
    // device platform info
    public class func devicePlatformInformation() ->NSString
    {
        let device = UIDevice.currentDevice()
        let platform = "\(device.systemName) \(device.systemVersion)"
        return platform
    }
    
    // device UUID
    public class func deviceId() -> NSString
    {
        var UUIDStr = NSString()
        if ASIdentifierManager.sharedManager().respondsToSelector(Selector("advertisingIdentifier"))
        {
            UUIDStr = ASIdentifierManager.sharedManager().advertisingIdentifier.UUIDString
        }
        
        if UUIDStr.length == 0
        {
            UUIDStr = UIDevice.currentDevice().identifierForVendor.UUIDString
        }
        
        return UUIDStr
    }
    
    // application version
    public class func appVersion() -> NSString
    {
        return NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString")as! String
    }
    
    // Appl build Number
    public class func buildNumber() -> NSString
    {
        return NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleVersion") as! String
    }
   
    /* It has crash TO_DO
    
    // get appplication name
    public class func appName() -> NSString
    {
        return NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleDisplayName") as String
    }*/
    
    // Email Validation
    public class func isValidEmail(emailStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(emailStr)
    }

    // Internet connection warning
    public class func showInternetConnectionWarning()
    {
        showAlertDialog(Constants().kWarning, alertMessage: Constants().kNoInternetConnectionMsg)
    }
    
    // show alert dialog
    public class func showAlertDialog(alertTitle: NSString, alertMessage: NSString)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let title = alertTitle as String
        let msg = alertMessage as String
        
        var alert = UIAlertController(title: title,
            message: msg,
            preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: Constants().kOk, style: UIAlertActionStyle.Default, handler: nil))
        appDelegate.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
}
