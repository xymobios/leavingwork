//
//  Constants.swift
//  TrackMyLocation
//
//  Created by Nidhi Sharma on 6/4/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

public struct Constants
{
    // App key
    let APP_KEY         =   "sensework06072015"
    
    // Urls
    var URL_CREATE_ACCOUNT         =  "http://www.xymob.com/s360/account/create"
    var URL_SEND_TRIGGER_INFO      =  "http://www.xymob.com/s360/trigger/message"
    var URL_TERMS_OF_USE           =  "http://www.sense360.com/leavingworkprivacy"
    
    // App keys
    var kToken          =   "token"
    var kSession        =   "session"
    var kPlatform       =   "platform"
    var kDeviceId       =   "device"
    var kAppVersion     =   "app-version"
    var kVersion        =   "version"
    var kAppCode        =   "app-code"
    
    var kMessage        =   "message"
    var kCode           =   "code"
    let kError          =   "Error"
    let kWarning        =   "Warning"
    
    let kUserInfo       =   "userInfo"
    let kUserPhone      =   "userPhone"
    let kUserPhoneDC    =   "userPhoneDC"
    let ksmsText        =   "smsText"
    let kStartTime      =   "startTime"
    let kEndTime        =   "endTime"
    let kTimeFormat     =   "HH"
    
    // Server Keys
    let kServerError    =   "error"
    let kServerEmail    =   "email"
    let kServerMsg      =   "msg"
    let kServerStatus   =   "status"
    
    // Constants
    var kRegistrationDone           =   "RegistrationDone"
    var kRecipeRegistrationDone     =   "RecipeRegistrationDone"
    let kUserEmail                  =   "userEmail"
    let kUserName                   =   "firstName"
    
    // Button Names
    let kCancel         =   "Cancel"
    let kOpenSettings   =   "Open Settings"
    let kOk             =   "Ok"
    let kRestart        =   "Re-start"
    let kPause          =   "Pause"
    
    
    // Request Time Interval
    var REQUEST_TIME_OUT : NSTimeInterval    =   60
    
    // Http Method Types
    var HTTP_METHOD_GET    =   "GET"
    var HTTP_METHOD_POST   =   "POST"
    var HTTP_METHOD_PUT    =   "PUT"
    var HTTP_METHOD_HEAD   =   "HEAD"
    var HTTP_METHOD_DELETE =   "DELETE"
    var HTTP_METHOD_PATCH  =   "PATCH"
    
    // Request Ids
    var REQUEST_ID_CREATE_ACCOUNT          =  1
    var REQUEST_ID_SEND_TRIGGER_INFO       =  2
    
    // Trigger place type
    let TRIGGER_PLACE_TYPE_POI          = 1
    let TRIGGER_PLACE_TYPE_PERSONALIZED = 2
    let TRIGGER_PLACE_TYPE_CUSTOM       = 3
    
    // Recipe related
    let kWorkTriggerRecipe      =   "WorkTriggerRecipe"
    let kSensingPaused          =   "SensingStopped"
    
    // Alert Message Strings
    var kNetworkFailed          =   "Network Failed, please try again later."
    var KEnterName              =   "Please enter your name."
    var KEnterEmail             =   "Please enter your email Id."
    var kEnterValidEmail        =   "Please enter valid email Id."
    var kEnterPassword          =   "Please enter password."
    var kAcceptTerms            =   "Please accept Terms of Use and Privacy Policy."
    var kErrorMessage           =   "Some error occured, please try again later."
    
    // SetPhoneScreen Related
    var kUserTextMessage        =   "Hey - just wanted to let you know I just left the office."
    var kEnterPhone             =   "Please enter phone number to whom text should be sent."
    var kEnterPhoneDC           =   "Please enter country code."
    var kEnterSMSText           =   "Please enter SMS text."
    var kSetStartTime           =   "Set start time"
    var kSetEndTime             =   "Set end time"
    
    
    // Location Permission Screen Related
    let kBGAccessDisbaledTitle      =   "Background Location Access Disabled"
    let kLocationDeniedMsg          =   "In order to automatically detect when you leave work, we need access to your location."
    
    let kNoInternetConnectionMsg    =   "We are unable to connect to the network. Please make sure that your device is connected to the internet."
    
    
}