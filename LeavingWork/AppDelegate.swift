//
//  AppDelegate.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/29/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import SenseSdk

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, ServerWrapperDelegate,RecipeFiredDelegate  {

    var window: UIWindow?
    let manager  = CLLocationManager()
    let backend = ServerWrapper()
    let constants = Constants()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        SenseSdk.enableSdkWithKey(constants.APP_KEY)
        
        //Crashlytics.startWithAPIKey("900122f21b9691eea58888c6f3794a56edd5a87e")
        
        manager.delegate = self
        
        // register recipe
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if !defaults.boolForKey(constants.kSensingPaused) &&
            defaults.boolForKey(constants.kRegistrationDone)
        {
            registeringRecipe()
        }
        
        setRootController()
        self.window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication)
    {
    }

    func applicationDidEnterBackground(application: UIApplication)
    {
    }

    func applicationWillEnterForeground(application: UIApplication)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if defaults.boolForKey(constants.kRegistrationDone)
        {
            if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedAlways
            {
                let locationPermissionController = storyboard.instantiateViewControllerWithIdentifier("LocationPermision") as! LocationPermissionScreen
                self.window?.rootViewController = locationPermissionController
                locationPermissionController.viewDidAppear(false)
            }
            else
            {
                if defaults.boolForKey(constants.kRecipeRegistrationDone)
                {
                    let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
                    self.window?.rootViewController = confirmationScreen
                    confirmationScreen.viewDidAppear(false)
                }
                else
                {
                    let phoneSetScreen = storyboard.instantiateViewControllerWithIdentifier("phoneScreen") as! UIViewController
                    self.window?.rootViewController = phoneSetScreen
                }
            }
        }
    }

    func applicationDidBecomeActive(application: UIApplication)
    {
    }

    func applicationWillTerminate(application: UIApplication)
    {
    }

    // MARK - CLLocation MAnager Delegate Methods
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        if status == CLAuthorizationStatus.AuthorizedAlways
        {
            NSLog("CLAuthorizationStatus > location is authorized")
            
            let defaults = NSUserDefaults.standardUserDefaults()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if defaults.boolForKey(constants.kRecipeRegistrationDone)
            {
                let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
                self.window?.rootViewController = confirmationScreen
                confirmationScreen.viewDidAppear(false)
            }
            else
            {
                let phoneSetScreen = storyboard.instantiateViewControllerWithIdentifier("phoneScreen") as! UIViewController
                self.window?.rootViewController = phoneSetScreen
            }
        }
        else if status == CLAuthorizationStatus.AuthorizedWhenInUse
        {
            NSLog("CLAuthorizationStatus > AuthorizedWhenInUse")
        }
        else if status == CLAuthorizationStatus.NotDetermined
        {
            NSLog("CLAuthorizationStatus > NotDetermined")
        }
        else if status == CLAuthorizationStatus.Denied
        {
            NSLog("CLAuthorizationStatus > Denied")
        }
    }
    
    // MARK - User Defined Functions
    
    func setRootController()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if defaults.boolForKey(constants.kRegistrationDone)
        {
            if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedAlways
            {
                let locationPermissionController = storyboard.instantiateViewControllerWithIdentifier("LocationPermision") as! UIViewController
                self.window?.rootViewController = locationPermissionController
            }
            else
            {
                if defaults.boolForKey(constants.kRecipeRegistrationDone)
                {
                    let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
                    self.window?.rootViewController = confirmationScreen
                    confirmationScreen.viewDidAppear(false)
                }
                else
                {
                    let phoneSetScreen = storyboard.instantiateViewControllerWithIdentifier("phoneScreen") as! UIViewController
                    self.window?.rootViewController = phoneSetScreen
                }
            }
        }
        else
        {
            let registrationScreen = storyboard.instantiateViewControllerWithIdentifier("RegistrationController") as! UIViewController
            self.window?.rootViewController = registrationScreen
        }
    }
    
    // MARK - SenseAPI Related
    
    func registeringRecipe()
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if (defaults.objectForKey(constants.kUserInfo) != nil)
        {
            let userInfo = defaults.objectForKey(constants.kUserInfo) as! NSDictionary
            
            var startDate = NSDate()
            var endDate = NSDate()
            
            if userInfo.objectForKey(constants.kStartTime) != nil
            {
                startDate = userInfo.objectForKey(constants.kStartTime) as! NSDate
            }
            
            if userInfo.objectForKey(constants.kEndTime) != nil
            {
                endDate = userInfo.objectForKey(constants.kEndTime) as! NSDate
            }
            
            defaults.setBool(false, forKey: constants.kSensingPaused)
            
            let calendar = NSCalendar.currentCalendar()
            let startDateComponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute, fromDate: startDate)
            let endDateComponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute, fromDate: endDate)

            let errorPointer = SenseSdkErrorPointer.create()
            let trigger = FireTrigger.whenExitsPersonalizedPlace(.Work, errorPtr: errorPointer)
            
            if let workTrigger = trigger
            {
                let workRecipe = Recipe(name: constants.kWorkTriggerRecipe,
                    trigger: workTrigger,
                    timeWindow: TimeWindow.create(fromHour: startDateComponents.hour, toHour: endDateComponents.hour,
                        errorPtr: errorPointer)!)
                SenseSdk.register(recipe: workRecipe, delegate: self, errorPtr: errorPointer)
            }
            
            if errorPointer.error != nil
            {
                NSLog("Error!: \(errorPointer.error.message)")
            }
            defaults.synchronize()
        }
    }
    
    // MARK - RecipeFiredDelegate Methods
    func recipeFired(args: RecipeFiredArgs)
    {
        var miliseconds = Int64(args.timestamp.timeIntervalSince1970 * 1000)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if (defaults.objectForKey(constants.kUserInfo) != nil)
        {
            let userInfo = defaults.objectForKey(constants.kUserInfo) as! NSDictionary
            
            var message = NSString()
            var phone = NSString()
            
            if (userInfo.objectForKey(constants.ksmsText) != nil)
            {
                message = userInfo.objectForKey(constants.ksmsText) as! String
            }
            if (userInfo.objectForKey(constants.kUserPhone) != nil &&
                userInfo.objectForKey(constants.kUserPhoneDC) != nil)
            {
                var dialingCode : String = userInfo.objectForKey(constants.kUserPhoneDC) as! String
                var number : String = userInfo.objectForKey(constants.kUserPhone) as! String
                phone = dialingCode + number
            }
            if args.triggersFired.count > 0
            {
                let triggerFired = args.triggersFired[0]
                if triggerFired.places.count > 0
                {
                    let place = triggerFired.places[0]
                    let transitionDesc = args.recipe.trigger.transitionType.description
                    
                    switch(place.type)
                    {
                    case .CustomGeofence:
                        if let geofence = place as? CustomGeofence
                        {
                            Helper.GetAppDelegate().backend.sendTriggerInfo(constants.TRIGGER_PLACE_TYPE_CUSTOM, place: geofence.customIdentifier, action: transitionDesc, message: message, phone: phone, timeStamp: miliseconds, delegate: self)
                        }
                        break;
                    case .Personal:
                        if let personal = place as? PersonalizedPlace
                        {
                            Helper.GetAppDelegate().backend.sendTriggerInfo(constants.TRIGGER_PLACE_TYPE_PERSONALIZED, place: personal.personalizedPlaceType.description, action: transitionDesc, message: message, phone: phone, timeStamp: miliseconds, delegate: self)
                        }
                        break;
                    case .Poi:
                        if let poi = place as? PoiPlace
                        {
                            Helper.GetAppDelegate().backend.sendTriggerInfo(constants.TRIGGER_PLACE_TYPE_POI, place: poi.types[0].description, action: transitionDesc, message: message, phone: phone, timeStamp: miliseconds, delegate: self)
                        }
                        break;
                    }
                }
            }
        }
    }
    
    // MARK - Server Wrapper Delegate Methods
    
    func triggerInfoSentSuccessfully(results: NSDictionary)
    {
        NSLog("Trigger Info Sent Successfully %", results)
    }
    
    func requestFailed(requestId: Int, errorInfo: NSDictionary)
    {
        NSLog("requestFailed with Error %", errorInfo)
    }
}

